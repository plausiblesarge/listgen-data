- Buy the [Sands of Inaros](@x) blueprint from [Baro Ki'Teer](@x) and craft it@tag{baro_buy_inaros_quest}
- After crafting, activate the [Sands of Inaros](@x) quest and listen to [Baro Kit'Teer](@x)'s inbox mesage
- Travel to Ara (Mars) to the Tomb to receive the [Inaros](@x) blueprint and the [Sacred Vessel](@x)
- Retrieve the urns and fulfill their inscriptions@ign
    - Fulfill the inscription on the [Sacred Vessel](@x) by killing either 60 [Detron Crewmen](@ex/Detron_Crewman), [Seeker](@x)s or [Volatile Runner](@x)s, depending on inscription, for the [Inaros Neuroptics](@x) blueprint
        - Return to the Tomb on Mars to progress the quest and receive a new urn.
            - It may be good to scan the [Feral Kavat](@x)'s during this mission@ign
    - Then, Fulfill the inscription on the [Sacred Vessel](@x) by killing either 20 [Railgun Moa](@x)s, [Hyekka Master](@x)s or [Brood Mother](@x)s, depending on inscription, for the [Inaros Chassis](@x) blueprint
        - Return to the Tomb on Mars to progress the quest and receive a new urn.
            - You will need to fight some [Tomb Guardian](@x)s
    - Then, Fulfill the inscription on the [Sacred Vessel](@x) by killing either 5 [Denial Bursa](@x)s, [Manic](@x)s or [Juggernaut](@x)s, depending on inscription, for the [Inaros Systems](@x) blueprint
        - Return to the Tomb on Mars to progress the quest and receive a new urn.
            - You will need to fight the [Tomb Protector](@x) to proceed. Be careful of the [Tomb Protector Effigy](@x)
        - Finish the mission and the quest will be completed.@tag{quest_completed_sands_of_inaros}

Do not delete or sell any part of the [Inaros](@x) blueprint or [Inaros](@x) himself, as there is way to repeat the quest@ign@imp
