- Buy the [Sands of Inaros](@x) blueprint from [Baro Ki'Teer](@x) and craft it@tag{baro_buy_inaros_quest}
- Complete the [Sands of Inaros](@x) quest to receive the blueprint and components to craft [Inaros](@x)@tag{quest_completed_sands_of_inaros}
- Craft Inaros@tag{craft_warframe_inaros}
    - [Forma](@x) as many times as necessary@tag{max_warframe_inaros}@nochange
    - Add a [Lens](@x)@tag{max_warframe_inaros}@nochange
    - Add an [Orokin Reactor](@x)@tag{max_warframe_inaros}@nochange
    - Add an [Exilus Adapter](@x)@tag{max_warframe_inaros}@nochange
- Get [Inaros](@x) to max rank@tag{max_rank_inaros}

Do not delete or sell any part of the [Inaros](@x) blueprint or [Inaros](@x) himself, as there is way to repeat the quest@ign@imp
