### Zaws ###

[Zaw](@x)s can exist in any combination, but only the strike of any given [Zaw](@x) is able to earn [Mastery Rank](@x) points.

- Reach Max Rank with the [Balla](@x) strike@tag{max_rank_balla}
- Reach Max Rank with the [Cyath](@x) strike@tag{max_rank_cyath}
- Reach Max Rank with the [Dehtat](@x) strike@tag{max_rank_dehtat}
- Reach Max Rank with the [Kronsh](@x) strike@tag{max_rank_kronsh}
- Reach Max Rank with the [Mewan](@x) strike@tag{max_rank_mewan}
- Reach Max Rank with the [Ooltha](@x) strike@tag{max_rank_ooltha}
- Reach Max Rank with the [Plague Keewar](@x) strike@tag{max_rank_plague_keewar} (no longer available)
- Reach Max Rank with the [Plague Kripath](@x) strike@tag{max_rank_plague_kipath} (no longer available)

### Amps ###

[Amp](@x)s can exist in any combination, but only the prism of any given [Amp](@x) is able to earn [Mastery Rank](@x) points.

@imp[Amp](@x)s only earn [Mastery Rank](@x) points when gilded!

- Reach Max Rank with the [Mote Amp](@x)@tag{max_rank_mote_amp} (gilded)
- Reach Max Rank with the [Raplax Prism](@x)@tag{max_rank_raplak} (gilded)
- Reach Max Rank with the [Shwaak Prism](@x)@tag{max_rank_shwaak} (gilded)
- Reach Max Rank with the [Granmu Prism](@x)@tag{max_rank_granmu} (gilded)
