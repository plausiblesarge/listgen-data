### Primary Weapons ###

- Reach Max Rank with [Boar Prime](@x)@tag{max_rank_boar_prime}
- Reach Max Rank with [Boltor Prime](@x)@tag{max_rank_boltor_prime}
- Reach Max Rank with [Braton Prime](@x)@tag{max_rank_braton_prime}
- Reach Max Rank with [Burston Prime](@x)@tag{max_rank_burston_prime}
- Reach Max Rank with [Cernos Prime](@x)@tag{max_rank_cernos_prime}
- Reach Max Rank with [Latron Prime](@x)@tag{max_rank_latron_prime}
- Reach Max Rank with [Paris Prime](@x)@tag{max_rank_paris_prime}
- Reach Max Rank with [Soma Prime](@x)@tag{max_rank_soma_prime}
- Reach Max Rank with [Sybaris Prime](@x)@tag{max_rank_sybaris_prime}
- Reach Max Rank with [Tiberon Prime](@x)@tag{max_rank_tiberon_prime}
- Reach Max Rank with [Tigris Prime](@x)@tag{max_rank_tigris_prime}
- Reach Max Rank with [Vectis Prime](@x)@tag{max_rank_vectis_prime}

### Secondary Weapons ###

- Reach Max Rank with [Akbolto Prime](@x)@tag{max_rank_akbolto_prime}
- Reach Max Rank with [Akbronco Prime](@x)@tag{max_rank_akbronco_prime}
- Reach Max Rank with [Aklex Prime](@x)@tag{max_rank_aklex_prime}
- Reach Max Rank with [Akstiletto Prime](@x)@tag{max_rank_akstiletto_prime}
- Reach Max Rank with [Ballistica Prime](@x)@tag{max_rank_ballistica_prime}
- Reach Max Rank with [Bronco Prime](@x)@tag{max_rank_bronco_prime}
- Reach Max Rank with [Euphona Prime](@x)@tag{max_rank_euphona_prime}
- Reach Max Rank with [Hikou Prime](@x)@tag{max_rank_hikou_prime}
- Reach Max Rank with [Lato Prime](@x)@tag{max_rank_lato_prime}
- Reach Max Rank with [Lex Prime](@x)@tag{max_rank_lex_prime}
- Reach Max Rank with [Sicarus Prime](@x)@tag{max_rank_sicarus_prime}
- Reach Max Rank with [Spira Prime](@x)@tag{max_rank_spira_prime}
- Reach Max Rank with [Vasto Prime](@x)@tag{max_rank_vasto_prime}

### Melee Weapons ###

- Reach Max Rank with [Ankyros Prime](@x)@tag{max_rank_ankyros_prime}
- Reach Max Rank with [Bo Prime](@x)@tag{max_rank_bo_prime}
- Reach Max Rank with [Dakra Prime](@x)@tag{max_rank_dakra_prime}
- Reach Max Rank with [Dual Kamas Prime](@x)@tag{max_rank_dual_kamas_prime}
- Reach Max Rank with [Fang Prime](@x)@tag{max_rank_fang_prime}
- Reach Max Rank with [Fragor Prime](@x)@tag{max_rank_fragor_prime}
- Reach Max Rank with [Galatine Prime](@x)@tag{max_rank_galatine_prime}
- Reach Max Rank with [Glaive Prime](@x)@tag{max_rank_glaive_prime}
- Reach Max Rank with [Kogake Prime](@x)@tag{max_rank_kogake_prime}
- Reach Max Rank with [Kronen Prime](@x)@tag{max_rank_kronen_prime}
- Reach Max Rank with [Nami Skyla Prime](@x)@tag{max_rank_nami_skyla_prime}
- Reach Max Rank with [Nikana Prime](@x)@tag{max_rank_nikana_prime}
- Reach Max Rank with [Orthos Prime](@x)@tag{max_rank_orthos_prime}
- Reach Max Rank with [Reaper Prime](@x)@tag{max_rank_reaper_prime}
- Reach Max Rank with [Scindo Prime](@x)@tag{max_rank_scindo_prime}
- Reach Max Rank with [Silva & Aegis Prime](@x)@tag{max_rank_silva_and_aegis_prime}
- Reach Max Rank with [Skana Prime](@x)@tag{max_rank_skana_prime}
- Reach Max Rank with [Venka Prime](@x)@tag{max_rank_venka_prime}
