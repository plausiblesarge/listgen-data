### Primary Weapons ###

- Reach Max Rank with [Corvas](@x)@tag{max_rank_corvas}
- Reach Max Rank with [Cyngas](@x)@tag{max_rank_cyngas}
- Reach Max Rank with [Dual Decursion](@x)@tag{max_rank_dual_decursion}
- Reach Max Rank with [Fluctus](@x)@tag{max_rank_fluctus}
- Reach Max Rank with [Grattler](@x)@tag{max_rank_grattler}
- Reach Max Rank with [Imperator](@x)@tag{max_rank_imperator}
- Reach Max Rank with [Phaedra](@x)@tag{max_rank_phaedra}
- Reach Max Rank with [Velocitus](@x)@tag{max_rank_velocitus}

### Melee Weapons ###

- Reach Max Rank with [Agkuza](@x)@tag{max_rank_agkuza}
- Reach Max Rank with [Centaur](@x)@tag{max_rank_centaur}
- Reach Max Rank with [Kaszas](@x)@tag{max_rank_kaszas}
- Reach Max Rank with [Knux](@x)@tag{max_rank_knux}
- Reach Max Rank with [Onorix](@x)@tag{max_rank_onorix}
- Reach Max Rank with [Rathbone](@x)@tag{max_rank_rathbone}
- Reach Max Rank with [Veritux](@x)@tag{max_rank_veritux}
