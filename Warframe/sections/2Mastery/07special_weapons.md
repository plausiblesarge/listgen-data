### [Syndicate](@x) Weapons ###

- Reach Max Rank with [Sancti Tigris](@x)@tag{max_rank_sancti_tigris}
- Reach Max Rank with [Sancti Castanas](@x)@tag{max_rank_sancti_castanas}
- Reach Max Rank with [Sancti Magistar](@x)@tag{max_rank_sancti_magistar}
- Reach Max Rank with [Secura Penta](@x)@tag{max_rank_secura_penta}
- Reach Max Rank with [Secura Dual Cestra](@x)@tag{max_rank_secura_dual_cestra}
- Reach Max Rank with [Secura Lectra](@x)@tag{max_rank_secura_lectra}
- Reach Max Rank with [Synoid Simulor](@x)@tag{max_rank_synoid_simulor}
- Reach Max Rank with [Synoid Gammacor](@x)@tag{max_rank_synoid_gammacor}
- Reach Max Rank with [Synoid Heliocor](@x)@tag{max_rank_synoid_heliocor}
- Reach Max Rank with [Telos Boltor](@x)@tag{max_rank_telos_boltor}
- Reach Max Rank with [Telos Akbolto](@x)@tag{max_rank_telos_akbolto}
- Reach Max Rank with [Telos Boltace](@x)@tag{max_rank_telos_boltace}
- Reach Max Rank with [Vaykor Hek](@x)@tag{max_rank_vaykor_hek}
- Reach Max Rank with [Vaykor Marelok](@x)@tag{max_rank_vaykor_marelok}
- Reach Max Rank with [Vaykor Sydon](@x)@tag{max_rank_vaykor_sydon}
- Reach Max Rank with [Rakta Cernos](@x)@tag{max_rank_rakta_cernos}
- Reach Max Rank with [Rakta Ballistica](@x)@tag{max_rank_rakta_ballistica}
- Reach Max Rank with [Rakta Dark Dagger](@x)@tag{max_rank_rakta_dark_dagger}

### Starter Weapons ###

- Reach Max Rank with [Mk1-Braton](@x)@tag{max_rank_mk1_braton}
- Reach Max Rank with [Mk1-Paris](@x)@tag{max_rank_mk1_paris}
- Reach Max Rank with [Mk1-Strun](@x)@tag{max_rank_mk1_strun}
- Reach Max Rank with [Mk1-Furis](@x)@tag{max_rank_mk1_furis}
- Reach Max Rank with [Mk1-Kunai](@x)@tag{max_rank_mk1_kunai}
- Reach Max Rank with [Mk1-Bo](@x)@tag{max_rank_mk1_bo}
- Reach Max Rank with [Mk1-Furax](@x)@tag{max_rank_mk1_furax}

### Wraith Weapons ###

- Reach Max Rank with [Gorgon Wraith](@x)@tag{max_rank_gorgon_wraith}
- Reach Max Rank with [Ignis Wraith](@x)@tag{max_rank_ignis_wraith}
- Reach Max Rank with [Karak Wraith](@x)@tag{max_rank_karak_wraith}
- Reach Max Rank with [Latron Wraith](@x)@tag{max_rank_latron_wraith}
- Reach Max Rank with [Strun Wraith](@x)@tag{max_rank_strun_wraith}
- Reach Max Rank with [Vulkar Wraith](@x)@tag{max_rank_vulkar_wraith}
- Reach Max Rank with [Lato Vandal](@x)@tag{max_rank_lato_vandal}
- Reach Max Rank with [Twin Vipers Wraith](@x)@tag{max_rank_twin_vipers_wraith}
- Reach Max Rank with [Viper Wraith](@x)@tag{max_rank_viper_wraith}
- Reach Max Rank with [Furax Wraith](@x)@tag{max_rank_furax_wraith}
- Reach Max Rank with [Machete Wraith](@x)@tag{max_rank_machete_wraith}

### Prisma Weapons ###

- Reach Max Rank with [Prisma Gorgon](@x)@tag{max_rank_prisma_gorgon}
- Reach Max Rank with [Prisma Grakata](@x)@tag{max_rank_prisma_grakata}
- Reach Max Rank with [Prisma Tetra](@x)@tag{max_rank_prisma_tetra}
- Reach Max Rank with [Prisma Angstrum](@x)@tag{max_rank_prisma_angstrum}
- Reach Max Rank with [Prisma Dual Cleavers](@x)@tag{max_rank_prisma_dual_cleavers}
- Reach Max Rank with [Prisma Obex](@x)@tag{max_rank_prisma_obex}
- Reach Max Rank with [Prisma Skana](@x)@tag{max_rank_prisma_skana}
- Reach Max Rank with [Prisma Veritux](@x)@tag{max_rank_prisma_veritux}

### Vandal Weapons ###

- Reach Max Rank with [Braton Vandal](@x)@tag{max_rank_braton_vandal}
- Reach Max Rank with [Dera Vandal](@x)@tag{max_rank_dera_vandal}
- Reach Max Rank with [Quanta Vandal](@x)@tag{max_rank_quanta_vandal}
- Reach Max Rank with [Snipetron Vandal](@x)@tag{max_rank_snipetron_vandal}
- Reach Max Rank with [Supra Vandal](@x)@tag{max_rank_supra_vandal}
- Reach Max Rank with [Prova Vandal](@x)@tag{max_rank_prova_vandal}
- Reach Max Rank with [Imperator Vandal](@x)@tag{max_rank_imperator_vandal}

### Dex Weapons ###

- Reach Max Rank with [Dex Sybaris](@x)@tag{max_rank_dex_sybaris}
- Reach Max Rank with [Dex Furis](@x)@tag{max_rank_dex_furis}
- Reach Max Rank with [Dex Dakra](@x)@tag{max_rank_dex_dakra}

### Milestone Rewards ###

- Reach Max Rank with [Azima](@x)@tag{max_rank_azima} (100 days)
- Reach Max Rank with [Zenistar](@x)@tag{max_rank_Zenistar} (300 days)
- Reach Max Rank with [Zenith](@x)@tag{max_rank_zenith} (500 days)
- Reach Max Rank with [Sigma & Octanis](@x)@tag{max_rank_sigma_and_octanis} (700 days)

### Other Weapons ###

- Reach Max Rank with [Mara Detron](@x)@tag{max_rank_mara_detron}
