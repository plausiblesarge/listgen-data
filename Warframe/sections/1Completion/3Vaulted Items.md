### Warframes ###

- Craft [Volt Prime](@x)@tag{craft_warframe_volt_prime}@readonly
    - Fully Upgrade [Volt Prime](@x)@tag{max_warframe_volt_prime}@readonly

### Archwings ###

- Craft [Odonata Prime](@x)@tag{craft_archwing_odonata_prime}@readonly
    - Fully Upgrade [Odonata Prime](@x)@tag{max_archwing_odonata_prime}@readonly
