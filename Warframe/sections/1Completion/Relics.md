### Lith Relics ###

- Lith G2@ign
    - Common@ign
        - [Bo Prime](@x) Blueprint@readonly@tag{component_weapon_bo_prime_blueprint}
        - [Latron Prime](@x) Stock@readonly@tag{component_weapon_latron_prime_stock}
        - [Wyrm Prime](@x) Cerebrum@readonly@tag{component_sentinel_wyrm_prime_cerebrum}
    - Uncommon@ign
        - [Frost Prime](@x) Systems@readonly@tag{component_warframe_frost_prime_systems}
        - [Loki Prime](@x) Chassis@readonly@tag{component_warframe_loki_prime_chassis}
    - Rare@ign
        - [Glaive Prime](@x) Blueprint@readonly@tag{component_weapon_glaive_prime_blueprint}

### Meso Relics ###

- Meso E1@ign
    - Common@ign
        - [Bo Prime](@x) Ornament@readonly@tag{component_weapon_bo_prime_ornament}
        - [Frost Prime](@x) Chassis@readonly@tag{component_warframe_frost_prime_chassis}
        - [Latron Prime](@x) Barrel@readonly@tag{component_weapon_latron_prime_barrel}
    - Uncommon@ign
        - [Forma](@x) Blueprint@ign
        - [Wyrm Prime](@x) Systems@readonly@tag{component_sentinel_wyrm_prime_systems}
    - Rare@ign
        - [Ember Prime](@x) Blueprint@readonly@tag{component_warframe_ember_prime_blueprint}
- Meso F3@ign
    - Common@ign
        - [Ember Prime](@x) Neuroptics@readonly@tag{component_warframe_ember_prime_neuroptics}
        - [Loki Prime](@x) Blueprint@readonly@tag{component_warframe_loki_prime_blueprint}
        - [Reaper Prime](@x) Handle@readonly@tag{component_weapon_reaper_prime_handle}
    - Uncommon@ign
        - [Forma](@x) Blueprint@ign
        - [Glaive Prime](@x) Disc@readonly@tag{component_weapon_glaive_prime_disc}
    - Rare@ign
        - [Frost Prime](@x) Blueprint@readonly@tag{component_warframe_frost_prime_blueprint}


### Neo Relics ###

- Neo E1@ign
    - Common@ign
        - [Frost Prime](@x) Neuroptics@readonly@tag{component_warframe_frost_prime_neuroptics}
        - [Loki Prime](@x) Neuroptics@readonly@tag{component_warframe_loki_prime_neuroptics}
        - [Wyrm Prime](@x) Blueprint@readonly@tag{component_sentinel_wyrm_prime_blueprint}
    - Uncommon@ign
        - [Forma](@x) Blueprint@ign
        - [Reaper Prime](@x) Blade@readonly@tag{component_weapon_reaper_prime_blade}
    - Rare@ign
        - [Ember Prime](@x) Blueprint@readonly@tag{component_warframe_ember_prime_blueprint}
- Neo F1@ign
    - Common@ign
        - [Ember Prime](@x) Chassis@readonly@tag{component_warframe_ember_prime_chassis}
        - [Sicarus Prime](@x) Barrel@readonly@tag{component_weapon_sicarus_prime_barrel}
        - [Sicarus Prime](@x) Blueprint@readonly@tag{component_weapon_sicarus_prime_blueprint}
    - Uncommon@ign
        - [Forma](@x) Blueprint@ign
        - [Bo Prime](@x) Handle@readonly@tag{component_weapon_bo_prime_handle}
    - Rare@ign
        - [Frost Prime](@x) Blueprint@readonly@tag{component_warframe_frost_prime_blueprint}


### Axi Relics ###


- Axi L1@ign
    - Common@ign
        - [Latron Prime](@x) Blueprint@readonly@tag{component_weapon_latron_prime_blueprint}
        - [Reaper Prime](@x) Blueprint@readonly@tag{component_weapon_reaper_prime_blueprint}
        - [Wyrm Prime](@x) Carapace@readonly@tag{component_sentinel_wyrm_prime_carapace}
    - Uncommon@ign
        - [Forma](@x) Blueprint@ign
        - [Glaive Prime](@x) Blade@readonly@tag{component_weapon_glaive_prime_blade}
    - Rare@ign
        - [Loki Prime](@x) Systems@readonly@tag{component_warframe_loki_prime_systems}
- Axi S2@ign
    - Common@ign
        - [Latron Prime](@x) Receiver@readonly@tag{component_weapon_latron_prime_receiver}
        - [Loki Prime](@x) Blueprint@readonly@tag{component_warframe_loki_prime_blueprint}
        - [Wyrm Prime](@x) Blueprint@readonly@tag{component_sentinel_wyrm_prime_blueprint}
    - Uncommon@ign
        - [Forma](@x) Blueprint@ign
        - [Ember Prime](@x) Systems@readonly@tag{component_warframe_ember_prime_systems}
    - Rare@ign
        - [Sicarus Prime](@x) Receiver@readonly@tag{component_weapon_sicarus_prime_receiver}
