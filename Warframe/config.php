<?php
$title = "Warframe Completion Sheet";
$desc = "Checklist for Warframe. For completionists";
$long_desc = "The following is a checklist and set of information I use when playing Warfrane to make sure I don't miss
    an item, warframe or important mod, etc. I hope you find it useful. A big thanks goes out to the community
    of contributors on the <a href=\"http://warframe.wikia.com/wiki/WARFRAME_Wiki\">Warframe Wiki</a> where
    some of this information is borrowed from.";
$long_desc_subtitle = "Complete as of Plains of the Eidolon update";
$default_link_location = "http://warframe.wikia.com/wiki/";
$default_link_space_character = "_";
$output = "warframe.html";
$template = "template/template.in"; # You shouldn't need to modify this
?>
