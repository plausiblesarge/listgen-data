@img{http://darksouls3.wiki.fextralife.com/file/Dark-Souls-3/cemetery_of_ash_map.jpg}

- Turn right and grab the [Soul of a Deserted Corpse](@x) (200)
- Pick up the [Ashen Estus Flask](@x)
- Go up the path on the right and fight the [Ravenous Crystal Lizard](@x) and pick up the [Soul of an Unknown Traveler](@x) (800)
- Activate the Cemetery of Ash bonfire and unlock the [Rest](@ex/gestures) gesture
- Pick up the 5 [Firebomb](@x)s to make the second-phase [Gundyr](@ex/Iudex+Gundyr) fight easier
- Jump on the Stone chest from the cliff and pick up the [Titanite Shard](@x)
- Kill [Iudex Gundyr](@x) and recieve the [Coiled Sword](@x)

#### After Gundyr ####

- Go down the path to the left and pick up the [Homeward Bone](@x) behind the [Grave Warden](@x)s
- Go up the path to the right and kill the [Starved Hound](@ex/Dog) and pick up the [Ember](@x)
- Kill the 2 [Grave Warden](@x)s above the entrance to the [Firelink Shrine](@x) and pick up the [Ember](@x)
- Pick up the [Homeward Bone](@x) from on top of a tombstone to the right after going above the [Firelink Shrine](@x) entrance
- Kill the [Sword Master](@x) to recieve the [Uchigatana](@x) and the [Master's Set](@x)
- Pick up the [East-West Shield](@x) from the tree near the [Sword Master](@x)

#### Delapitated Tower ####

Once you have the [Tower Key](@x), you can access the roof and the dilapitated tower behind the [Firelink Shrine](@x)

- Pick up the [Soul of a Deserted Corpse](@x) (200) next to the entrance to the dilapitated tower
- Jump down from the tower bridge onto the roof and kick down the shortcut ladder
- Jump down from the roof onto the section next to the entrance to the tower, to pick up 3 [Homeward Bone](@x)s from the roof
- Kill the [Crystal Lizard](@x) behind the tower for a [Twinkling Titanite](@x)
- Pick up the [Estus Shard](@x) next to [Pickle-Pee](@ex/Pickle+Pee,+Pump-a-Rum+Crow)
- Go through the illusory wall at the end of the walkway to find a chest containing the [Covetous Silver Serpent Ring](@x)
- Pick up the [Fire Keeper Soul](@x) from the top of the tower
- Jump down the coffins to get the [Estus Ring](@x)
- Jump down the other side to get the [Fire Keeper Set](@x)
