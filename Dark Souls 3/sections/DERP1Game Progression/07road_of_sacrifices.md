@img{http://darksouls3.wiki.fextralife.com/file/Dark-Souls-3/road_of_sacrifices_map.jpg}

- Activate the [Road of Sacrifices](@x) bonfire
- Drop down the cliff next to the first 2 [Birdmen](@ex/Corvian) and pick up the [Shriving Stone](@x)
- [Soul of an Unknown Traveler](@x) (800) can be found near a broken wagon

Move past the first courtyard and drop down to the archway leading to the bridge

- Kill the [Mad Woman](@ex/madwoman) and pick up the [Butcher Knife](@x)
- Loot the corpse at the top of the cliff with the [Mad Woman](@ex/madwoman) for a [Brigand Axe](@x)
- Continue down the cliff path to find the [Brigand Set](@x)
- At the end of the path is the [Brigand Twindaggers](@x)
- Walk up the path on the left going above the archway to find a [Titanite Shard](@x)
- Cross the bridge filled with [Birdmen](@ex/Corvian) to find a corpse with an [Ember](@x)
- Drop down below the bridge to find a [Braille Divine Tome of Carim](@x) guarded by 2 [Starved Hound](@ex/Dog)s
- Next to the [Braille Divine Tome of Carim](@x) is [Morne's Ring](@x)

#### Halfway Fortress ####

- Activate the Halfway Fortress bonfire
- Talk to [Horace](@x) to gain access to the [Blue Sentinels](@x)
- Talk to [Anri of Astora](@x) to learn about the Crucifixion Woods. Talk to her again to learn about [Horace](@x)

