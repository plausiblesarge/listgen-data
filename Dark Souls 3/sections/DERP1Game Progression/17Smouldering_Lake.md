#### In the Water ####

The water is a large open area filled with items. Watch for the ballista raining arrows on you

- In the lake is a corpse holding a [Large Titanite Shard](@x)
- In the lake is another corpse holding a [Large Titanite Shard](@x)
- In the lake yet another corpse holds a [Large Titanite Shard](@x)
- Up against a tree is a corpse holding a [Large Titanite Shard](@x)
- A few [Giant Enemy Crabs](@ex/giant+crab) guard a [Chaos Gem](@x)
- On the left side of the lake is a brick wall which can be broken with a ballista arrow. Behind the wall is the [Speckled Stoneplate Ring](@x)
- @ignAt the edge of the lake is a [Giant Lightning Trouser Snale](@ex/lightning+worm)
    - Nearby is a corpse holding a [Large Titanite Shard](@x)
    - Nearby is another corpse holding the [Shield of Want](@x)
    - Kill the [Penis Snake](@ex/lightning+worm) for the [Lightning Stake](@x) miracle and an [Undead Bone Shard](@x)
        - @ignYou can do this using the Ballista if you stand behind the small rock near the [Cock Monster](@ex/lightning+worm)
- Go up the ramp near the [Chinpokomon](@ex/lightning+worm) to fight the [Fire Demon](@x)
- Nearby to the ramp is a small rocky outcrop. At the top is a corpse holding a [Large Titanite Shard](@x)
    - After this outcrop is a bonfire and a hallway going to the Catacombs Section


#### Horace's Cave ####

Go through the cave to the right hand edge of the lake.

- Kill the [Crystal Lizard](@x) for a [Twinkling Titanite](@x)
- Kill the second [Crystal Lizard](@x) for a [Titanite Chunk](@x)
- Kill Horace to complete [Anri of Astora](@x)s questline.
    - He will drop the [Llewellyn Shield](@x)
    - @imp@ignIf you see [Horace](@x) and don't kill him but tell [Anri of Astora](@x) where he is, she will hollow and you will fail her questline
- Against the wall is a corpse holding 2 [Yellow Bug Pellet](@x)s
- The corpse behind [Horace](@x) holds a [Large Titanite Shard](@x)
- Another corpse nearby also holds a [Large Titanite Shard](@x)

#### Catacombs Section ####

TODO: This
