@img{http://darksouls3.wiki.fextralife.com/file/Dark-Souls-3/catacombs_of_carthus_map_dks3.jpg}

There is a shorctut to the right. To the left is a path leading to a bridge.

- Before the bridge is a corpse holding 2 [Carthus Rouge](@x)
- Across the bridge is a room with stears going up and down. Take the stairs going up.@ign
    - At the end of the path is a [Soul of a Nameless Soldier](@x) (1000)
    - At the other end is a ladder going down to a [Soul of a Nameless Soldier](@x) (1000)
- In the next room is an illusory wall leading to the [Carthus Pyromancy Tome](@x)
- Next to the illusory wall is a corpse holding a [Sharp Gem](@x)
- Through the door is a door leading to a room with a large ramp. Behind a wall is a corpse holding a [Titanite Shard](@x)
- Through the nearby door is a path leading to [Anri of Astora](@x). Talk to her to continue her quest line.
    - She will tell you to find [Horace](@x). He is in the [Smouldering Lake](@x)@ign
    - Past [Anri of Astora](@x) is a small area with a [Crystal Lizard](@x) which drops a [Twinkling Titanite](@x) and a corpse holding 3 [Bloodred Moss Clump](@x)


#### Down the Ramp ####

Watch for the big ball! Move to the bottom of the ramp.

- On the middle of the ramp is a corpse containing an [Ember](@x)
- Move through the next area and move behind the large collection of pots to find the [Carthus Milkring](@x)
- In the next area is a corpse containing a [Large Soul of an Unknown Traveler](@x) (2000)
- Behind the hole in the ground is a corpse holding an [Ember](@x)
- In the next room is a corpse holding a [Large Titanite Shard](@x). Dodge the ball
- The bonfire is just around the corner
- Down the stairs, to the left of the rats, is a corpse holding 2 [Titanite Shard](@x)s
- At the end of the hall is a path leading to a [Crystal Lizard](@x) holding a [Fire Gem](@x)
    - Nearby is a corpse holding 3 [Yellow Bug Pellet](@x)s
    - There is a path leading to the bridge area@ign
- To the right are some rats and a path leading down to some wheel skeletons@ign
    - Halfway down the path is a corpse holding 2 [Titanite Shard](@x)s. Watch for the slime!
    - At the end of the path is the [Carthas Bloodring](@x)

At the path in the middle, near where the ball hits the wall, are some stairs going upwards. There is a secret wall next to the stairs which leads to more stairs, which go to the same area

- At the top of the secret stairs are the [Grave Warden's Ashes](@x)
- On a corpse is a [Large Titanite Shard](@x)
- Near the undead archer is 2 [Carthus Rouge](@x)


#### Bridge Area ####

- The path down the middle goes to a small area where some Skeletons guard 2 [Black Bug Pellet](@x)s and a [Large Soul of a Nameless Soldier](@x)
    - Nearby is a gate opening a shortcut from the bridge area back to the bonfire area.
    - Next to the gate is an [Ember](@x)
- You will be attacked by [Knight Slayer Tsorig](@x)
    - If he defeats you, you will get the [My Thanks](@ex/gestures) gesture
    - If you defeat him, he will drop the [Knight Slayer's Ring](@x)
- @ignTalk to [Anri of Astora](@x) about [Horace](@x). This is not required to progress her quest line. You should talk to her again after defeatung [Horace](@x)
- @ignBreak the bridge to stop the skeletons from chasing you
- After the bridge area, touch the goblet and defeat [High Lord Wolnir](@x)
    - He will drop [His Soul](@ex/soul+of+high+lord+wolnir)
    - Light the [High Lord Wolnir](@x) bonfire
    - The [Grave Warden Pyromancy Tome](@x) is in front of [High Lord Wolnir](@x)

Take the stairs to the [Irithyll of the Boreal Valley](@x). You can also go below the bridge to get to the [Smouldering Lake](@x)


#### Smouldering Lake Entrance ####

- Kill the [Fire Demon](@x) for the [Soul of a Demon](@x)
- In the next area is a corpse holding a [Large Titanite Shard](@x)
- Nearby is another corpse holding a [Large Soul of a Nameless Soldier](@x) (2000)
- Down the stairs in the next room is a corpse holding the [Witch's Ring](@x) and the [Old Sage's Blindfold](@x)
- Light the bonfire through the right passageway
