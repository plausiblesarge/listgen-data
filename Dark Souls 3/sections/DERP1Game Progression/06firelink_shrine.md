- Give the [Mortician's Ashes](@x) to the [Shrine Handmaiden](@x) and buy any extra items you want
	- [Grave Key](@x) (1500)
    - 3 [Ember](@x)s (2500 each)
- Upgrade your [Estus Flask](@x) if you have not done so already
- Burn your [Bome Shard](@x)(s) if you have not done so already
- Transpose the [Soul of Boreal Valley Vordt](@x) or use it to acquire 2000 souls
- Transpose the [Soul of the Rotted Greatwood](@x) or use it to acquire 3000 souls
- New Dialog is available@ign
	- [Hawkwood](@x) will talk about the Cathedral of the Deep. Talk to him again to learn about the [Road of Sacrifices](@x)
        - He will give you a [Heavy Gem](@x) after you have defeated the [Curse Rotted Greatwood](@x) or lit the [Road of Sacrifices](@x) bonfire
	- Talk to [Cornyx](@ex/Cornyx+of+the+great+swamp) for a [Pyromancy Flame](@x). Talk to him again for the [Welcome](@ex/gestures) gesture
	- [Irina of Carim](@x) will talk about her purpose
	- [Yoel of Londor](@x) will talk about sorcery and give you the [Beckon](@ex/gestures) gesture
	- [Ludleth of Courland](@x) will talk about Soul Transposition. Give him the [Transposing Kiln](@x)
	- [Leonhard the Ringfinger](@x) will give you the [Lift Chamber Key](@x) if you have a [Pale Tongue](@x) in your inventory
	- Give [Loretta's Bone](@x) to [Greirat](@x) to finish his quest
