- A corpse on the right after some [Lycanthrope Hunter](@x)s holds a [Titanite Shard](@x)
- Stick to the right hand path, and go to the crumbling ruin. Stick to the right to avoid the [Silver Knight](@x). Up against the wall is a corpse containing the [Sellsword Set](@x)
- On the ground below it are the [Sellsword Twinblades](@x)
- At the end of the hall is the [Farron Coal](@x)
- On the left edge of the area is a body containing a [Titanite Shard](@x)
- further down the path, some [Poison Blobs](@ex/Poisonhorn+Bug) guard another [Titanite Shard](@x)
- Further along the left path is a clump of trees with a body containing the [Twin Dragon Greatshield](@x)
- Next to the archway is a body containing a [Fading Soul](@x) (50)
- Activate the Cricifixion Woods bonfire
- Kill Holy Knight Hodrick

#### In the Water ####

- Near the Keep in the water is a corpse containing 2 [Green Blossom](@x)s
- Kill the [Giant Enemy Crab](@ex/Greater+Crab) near Farron Keep for the [Great Swamp Ring](@x)
- Near Farron Keep is a corpse containing 4 [Green Blossom](@x)
- Near the large tree in the deep water (where the [Greater Crab](@x) was located) is the [Conjurator Set](@x) and the [Great Swamp Pyromancy Tome](@x)
- [Grass Crest Shield](@x) near the first big crab behind a tree

#### Land Mass near Farron Keep ####

- At the very edge of the water is a corpse containing the [Fallen Knight Set](@x)
- On the ground to the left of the Farron Keep entrance is a body containing a [Large Soul of an Unknown Traveler](@x) (1000)
- Kill [Yellowfinger Heysel](@x) and get the [Xanthous Crown](@x) and the [Heysel Pick](@x)
- Kill the Watchdogs of Farron NPCs and loot the [Great Club](@x) and the [Exile Greatsword](@x)
- 2 [Homeward Bone](@x)s can be found past the 2 NPC Watchdogs of Farron

#### Area Outside the Castle ####

- At the front of the keep is a large tree, with a body containing a [Soul of an Unknown Traveler](@x) (800)
- Through the arch is a fire. Next to the fire is a corpse holding an [Ember](@x)
- In the corner of this area is an [Estus Shard](@x)
- In the small flooded basement area accessible by the waterway is a corpse containing the [Sorcerer Set](@x)
- Across from the corpse is another corpse contianing the [Sage Ring](@x)

#### In the Castle ####

- At the top of the spiral stairs near the entrance is a drop down ledge, containing a corpse with a [Ring of Sacrifice](@x)
- The spiral staircase also contains a walkway leading to the [Golden Falcon Shield](@x)
- Kill the [Crystal Lizard](@x) in the main hallway for a [Crystal Gem](@x)
- In the left most atrium is a corpse with the [Heretic's Staff](@x)
- Go upstairs and find [Orbeck of Vinheim](@x). Talk to him and he will return to [Firelink Shrine](@x)
- While you are up there, also pick up the 2 [Blue Bug Pellet](@x)s on the broken staircase
- Kill the Crystal Sage
    - Activate the Crystal Sage bonfire

#### Past the Crystal Sage ####

- Move up the pathway then drop down to the [Crystal Lizard](@x) on the left, who has a [Twinkling Titanite](@x)
- Drop down another level and kill the other [Crystal Lizard](@x) who also has a [Twinkling Titanite](@x)
- The [Herald Set](@x) is behind the [Evangelist](@x)
