#### Cleansing Chapel ####

- Light the Cleansing Chapel bonfire
- In the corner of a room is the [Notched Whip](@x)
- Talk to [Slave Knight Gael](@x) to be transported to the [Painted World of Ariandel](@x) (not recommended unless you are level 60-70 or higher)
- Free [Siegward of Catarina](@x) by taking all 4 of his armor pieces to him in the well outside the Cleansing Chapel
    - He will give you the [Rejoice](@ex/gestures) gesture@tag{gesture_rejoice}

#### The Graveyard and Cathedral Outskirts ####

- On the path leading up to the graveyard, many hollows are gathered around an [Estus Shard](@x)
- The [Astora Greatsword](@x) is on a hill next to a grave
- After crossing the bridge, in the area where the giant is throwing spears there is a [Large Soul of an Unknown Traveler](@x) (1000)
- There is a second [Large Soul of an Unknown Traveler](@x) (1000) nearby
- 3 [Repair Powder](@x)s can be found on a corpse in the same area
- An [Undead Bone Shard](@x) can be found on the end of the overhanging coffin
- There are 2 [Young White Branch](@x)es around the birch tree

After continuing, The stairs on the left go upwards to some big doors. The path around is on the right, but for now we want to finish exploring this area.

- Behind some boxes on the left are 2 [Rusted Coin](@x)s
- A [Red Bug Pellet](@x) is in the hallway leading to the next area

After finishing here, move back down the stairs and to the tower on the other side

- On the tower railing to the left of the entrance is a body holding a [Titanite Shard](@x)
- Inside the tower is a corpse holding the [Curse Ward Greatshield](@x)
- At the bottom of the tower steps, kick down the shortcut ladder and go down

#### Cathedral Outskirts - Ground Level #### 

- Immediately outside the tower, proceed forward up the stairs. At the top, to the right is a corpse in a window with a [Titanite Shard](@x)
- Beyond this area is a [Rabid Crystal Lizard](@x) who drops a [titanite Scale](@x)
- Nearby is a [Crystal Lizard](@x) which drops a [Twinkling Titanite](@x)
- Near the [Crystal Lizard](@x) is a set of stairs leading to a body with a [Titanite Shard](@x)
- There is a small alcove nearby where the [Rabid Crystal Lizard](@x) was originally resting. Inside is a [Poisonbite Ring](@x)
- Continuing along the main path reveals another [Crystal Lizard](@x) which provides another [Twinkling Titanite](@x)
- In the corner next to the stairs is a [Titanite Shard](@x)
- Down the stairs and around the corner is the [Saint Tree Bellvine](@x)

The final area at the ground level is a flooded graveyard. This serves as an alternate entrance

#### Cathedral Roof ####

- On the roof, move into the cemicircle area and pick up the [Large Soul of an Unknown Traveler](@x) (1000)

Go down to the area with all the archers

- Go up the small flight of stairs. On the left, an [Evangelist](@x) is guarding 3 [Undead Hunter Charm](@x)s
- On the right is 3 [Red Bug Pellet](@x)s
- In the small house on the left afterwards, pick up the [Soul of a Nameless Soldier](@x) (400)

Move through the next section, killing both of the [Grave Warden](@x)s who are together. Move up to the big doors

- Before you open them go to the right and kill the mob guarding the [Ember](@x)

#### Inside the Church ####

- Enter the church through the large doors and turn left. At the end o the hall is a [Duel Charm](@x). Watch for the poison trap!
- Move past the small deacons room and take the elevator to the left. Open the shortcut to the bonfire
- In the room before the giant is a staircase going down to a room with an [Evangelist](@x). Behind the evangelist is a [Deep Gem](@x)
- Move past the giant. In the middle of the semicircle there is a [Soul of a Nameless Soldier](@x) and [Lloyd's Sword Ring](@x)
- Nearby is a corpse holding 6 [Exploding Bolt](@x)s

On the other side of the semicircle with the giant are some stairs leading down 2 levels

- Head down to the middle level. On a ledge is the [Seek Guidance](@x) miracle. Watch for the falling [Slime](@x)!
- Head up the ladder on the middle level. At the top, an [Evangelist](@x) guards an [Ember](@x). This is a good time to get revenge on the [Arbalest](@x) guy!
- On the lower level, a [Mimic](@x) holds a [Deep Braille Divine Tome](@x)
- In the room to the left, behind the [Cathedral Knight](@x) is a small room where a [Deep Accursed](@x) will drop [Aldrich's Sapphire](@x)
- Behind the Deep Accursed is an [Ember](@x)

Taking the stairs out of this room will lead to a large flooded chamber with a wing on each side. Cross the flooded chamber and go to the other wing

- [Longfinger Kirk](@x) will invade you. Kill him for [His Armor](@ex/Armor+of+thorns+set) to appear in Rosaria's Bed Chamber
- Follow the path down from this wing to a bonfire ehortcut
- Next to the shortcut, take the elevator up and kill the archer, then take the ladder up and kill the [Deacon](@x), who will drop the [Deep Ring](@x)

Jump off the roof and move up the path

#### Cathedral Roof, Second Level ####

- After moving up the roof, one of the small towers contains an [Arbalest](@x)
- At the end of the roof section is a [Pale Tongue](@x)

Move inside to the criss-crossing section above the floor of the main cathedral

- Behind the Sword-Wielding [Cathedral Knight](@x) is a corpse containing a [Blessed Gem](@x)

#### Alcove and Main Entrance ####

- A Corpse against a wall up the stairs holds 3 [Dual Charm](@x)s
- Open up the main door to open a shortcut to the graveyard area
    - @ign@impThis will also allow [Patches](@ex/Unbreakable+Patches) to appear near Rosarias Bed Chamber
    - @ignYou can walk across the raised pathway to Rosarias Bed Chamber
    - Activate [Patches](@x) questline@tag{patches_available}
        - Tell [Patches](@x) you know who he is to receive the [Prostration](@ex/gestures) gesture@tag{gesture_prostration}
        - He will also give you a [Rusted Coin](@x)

#### Rosarias Bed Chamber ####

@impTaking the rafters shortcut to Rosarias Bed Chamber will prevent [Patches](@ex/Unbreakable+Patches) from appearing. You will have to encounter him later at [Firelink Shrine](@x) to continue his quest line. You will miss out on a [Rusted Coin](@x) by doing this, but you will still receive everything else from [Patches](@ex/Unbreakable+Patches)

- Pull the lever to raise the middle section of the bridge
- Light Rosarias Chamber bonfire
- Next to the bonfire is the [Armor of Thorns Set](@x) set if you killed [Longfinger Kirk](@x) in the main cathedral area
- The slug outside Rosarias Bed Chamber drops the [Red Sign Soapstone](@x) on death

#### The 2 Giants ####

- Near the second giant is a lever to raise the third pathway
- Both [Giants](@ex/Giant+Slave) will drop a [Large Titanite Shard](@x) and 4 [Dung Pie](@x)s on death
- The [First Giant](@ex/Giant+Slave) is guarding the [Maiden Set](@x) 
    - And a [Soul of a Nameless Soldier](@x) (2000)
    - And a [Large Soul of an Unknown Traveler](@x) (1000)
    - Open the door in front of the [Giant](@ex/Giant+Slave) to find a courtyard with the [Saint Bident](@x),2 [Homeward Bone](@x)s and a gorgeous view
- The [Second Giant](@ex/Giant+Slave) is guarding the [Drang Armor Set](@x) and the [Drang Hammers](@x)
    - He is also guarding a [Pale Tongue](@x)
    - And a [Large Soul of an Unknown Traveler](@x) (1000)

#### [Deacons of the Deep](@x) ####

- On the alcove before the [Deacons of the Deep](@x) fog wall, there is a corpse holding an [Ember](@x)
- Take the lift shortcut up to the Main Entrance Alcove area
- Kill the [Deacons of the Deep](@x)
    - Killing the [Deacons of the Deep](@x) before killing [Longfinger Kirk](@x) will prevent him from appearing@imp@ign
    - They will drop [their soul](@ex/Soul+of+the+deacons+of+the+deep) and the [Small Doll](@x)@ign
    - Activate the [Deacons of the Deep](@x) bonfire
    - After resting at the bonfire, the [Archdeacon Set](@x) is next to the big tomb
