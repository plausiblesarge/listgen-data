- Upgrade your [Estus Flask](@x) if you have not done so already
- Burn your [Undead Bone Shard](@x)(s) if you have not done so already
- Transpose [Soul of High Lord Wolnir](@x) or use it to acquire 10,000 souls
- Transpose [Soul of a Demon](@x) or use it to acquire 20,000 souls
- Give the [Grave Warden Pyromancy Tome](@x) to [Irina of Carim](@x) or choose not to
    - @ign@impThis will immediately fail [Irina of Carim](@x)s questline. It is recommended to instead hold on to this item and give it to [Karla](@x) later on instead.
    - @ignCornyx will not accept this tome
