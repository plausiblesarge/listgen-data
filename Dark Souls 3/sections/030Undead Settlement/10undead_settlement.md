#### Collapsed Bridge ####

- Use the [Small Lothric Banner](@x) to fly down to the foot of the wall and activate the bonfire
- [Large Soul of a Deserted Corpse](@x) (400) to the left of the bonfire
- 2 [Alluring Skull](@x)s guarded by 2 [Starved Hounds](@ex/Dog)
- Take [Yoel of Londor](@x) into your service. He will teleport back to [Firelink Shrine](@x)
- 2 [Homeward Bones](@x) on the bridge near [Yoel of Londor](@x)
- Go through the gate and activate the [Undead Settlement](@x) bonfire

#### In the House ####

- [Small Leather Shield](@x) on a hanging corpse near the entrance
- 2 [Charcoal Pine Bundle](@x)s just down the stairs from the entrance. Be careful of an ambush by a [Thrall](@x)
- 2 [Repair Powder](@x) on the balcony
	- Remember to attack the corpse containing [Loretta's Bone](@x) so that it can be retrieved later@ign
- 2 [Charcoal Pine Bundle](@x)s on the ground floor

#### Courtyard Outside ####

- [Loretta's Bone](@x) on the ground
- [Soul of an Unknown Traveler](@x) (800) near the gate
- An [Estus Shard](@x) is next to the fire guarded by a [Evangelist](@ex/Cathedral+Evangelist)
- An [Ember](@x) behind the fire
- [Titanite Shard](@x) near the right hand entrance

Go into the small building down the left side of the courtyard

- 2 [Charcoal Pine Resin](@x)s against a wall guarded by a [Cage Spider](@x)
- [Large Soul of a Deserted Corpse](@x) (400) on a hanging corpse above the stairs
- Drop down the hole to obtain the [Warrior of Sunlight](@x) covenant and access to [Estus Soup](@x)

Go through the house to the courtyard outside

- A [Soul of an Unknown Traveler](@x) (800) is hidden behind some breakable debris near the cliff face
- A [Peasant Hollow](@x) will break through a door. Inside a [Thrall](@ex/Hollow+Slave) guards a [Whip](@x)

Go down towards the evangelist

- A [Titanite Shard](@x) can be found on the raised up area where the [Evangelist](@ex/Cathedral+Evangelist) is guarding
- A [Titanite Shard](@x) is hidden down a small path near the cliff
- Go over the roof near the evangelist and drop down to the roof with a corpse holding 2 [Rusted Coin](@x)s
- Going through the building at the end of the courtyard will take you to a clearing. Turn right to find a [Crystal Lizard](@x) with a [Sharp Gem](@x)
- Activate the Dilapitated Bridge bonfire

#### Bridge, Barn and Bridge Underside ####

Cross the bridge going down the right hand side of the courtyard

- [Large Soul of a Deserted Corpse](@x) (400) inside the barn
- Open the shortcut to the Bridge Underside bonfire
- Just past the shortcut door is a [Caduceus Round Shield](@x) on a small hill to the right
- Cross the small bridge and jump down onto the cliff. There is a [Titanite Shard](@x) at the end
- Activate the Bridge Underside bonfire

Move through the building and kill the hollows on the balcony

- Move to the top of the building to find and free [Cornyx of the Great Swamp](@x)
	- There is also a [Hand Axe](@x) next to [Cornyx](@ex/Cornyx+of+the+great+swamp)
- To the left of Cornyx is a hanging corpse holding a [Partizan](@x). You may need to use a [Firebomb](@x) to get it down
- Drop down and move to the Gallows to find a [Large Soul of an Unknown Traveler](@x) (1000). Watch out for the [Pot Guy](@ex/Hollow+Manservant)!
- The [Fire Clutch Ring](@x) is on a small overlook behind the gallows

Smash the barrels before the barn entrance to get behind the barn

#### Behind the Barn ####

- [Fading Soul](@x) (50) on the ground between the 2 houses
- 2 [Homeward Bone](@x)s on the roof of the house with the ladder
- [Plank Shield](@x) on the ground behind the house with the ladder
- 6 [Firebomb](@x)s on top of the barn
- Drop down off the right side of the house and knock down the hanging corpse. It contains a [Flame Stoneplate Ring](@x), which may need to be picked up later@tag{flame_stoneplate_ring}
- Optionally join the [Mound Makers](@x) covenant by examining the cage bearer nearby.
	- If you join the covenant, there is a [Wargod Wooden Shield](@x) next to [Holy Knight Hodrick](@x)@tag{wargod_wooden_shield}
- Talking to Hodrick 3 times will give you a [Homeward Bone](@x).

Since you cannot leave this area, you can either use a [Homeward Bone](@x) or save and quit, which will take you back to behind the barn

#### The Sewers ####

You can access the sewers by leaving from the Bridge Underside bonfire and moving down the stairs past the clearing with the [Pot Guy](@ex/Undead+Manservant)

- [Caestus](@x) on a corpse surrounded by rats
- [Bloodbite Ring](@x) dropped by the [Giant Rat](@ex/Hound-Rat)
- Open the sewer shortcut to the Dilapitated Bridge bonfire

#### Dilapitated Bridge ####

- You will be invaded by [Holy Knight Hodrick](@x) if you are Embered. He is a hard fight, so don't be afraid to come back later, or kick him off the edge. He drops a [Vertebrae Shackle](@x)

There will be a giant throwing spears in this area. It may be wise to complete the Bell Tower first to make the giant friendly.

- Down the path you will come to a white birch tree, with 3 [Young White Branch](@x)es at it's base.
- Next to the tree is a corpse with a [Reinforced Club](@x)
- Next to the tree is a corpse with an [Ember](@x)
- Next to the tree is a corpse with a [Large Soul of a Deserted Corpse](@x) (400)
- Next to the tree is a corpse with a [Fading Soul](@x)
- Near the tree, jump across a small ledge to find an [Undead Bone Shard](@x)
- Take the pathway up to the left of the house to find the [Cleric Set](@x) and a [Blue Wooden Shield](@x). You will be ambushed by a [Thrall](@ex/Hollow+Slave) so be careful!
- [Mortician's Ashes](@x) are nearby on a corpse
- Now go back to the house. At the back of the house on the top level is a [Great Scythe](@x)
- Open the Shortcut door to the [Curse-Rotted Greatwood](@x)
- Kill the [Curse-Rotted Greatwood](@x).
    - @ign@impThis will prevent you joining the [Mound Makers](@x) covenant if you haven't already!
	- The [Curse-Rotted Greatwood](@x) drops the [Transposing Kiln](@x) as well as [His Soul](@ex/soul+of+the+rotted+greatwood)@ign
	- Activate the [Curse-Rotted Greatwood](@x) bonfire to have easy access to the [Mound Makers](@x) covenant
    - Be sure to pick up the [Wargod Wooden Shield](@x)@tag{wargod_wooden_shield} 
- Just outside the [Curse-Rotted Greatwood](@x) shortcut door is an [Ember](@x) next to a tree guarded by a [Dog](@x)

#### Sewers Courtyard ####

- From the sewers cross the small bridge guarded by 3 [Hollow Manservant](@x)s amd pick up the [Ember](@x)
- In front of the small house after the bridge is a [Large Soul of a Deserted Corpse](@x) (400)
- Behind the house are 2 [Dog](@x)s guarding 3 [Alluring Skull](@x)s
- Remember to talk to [Eygon of Carim](@x)
- Pick up the [Flame Stoneplate Ring](@x) from here if you haven't already@tag{flame_stoneplate_ring}

#### The Giants Tower ####

- Enter the tower and talk to [Siegward of Catarina](@x) a few times to begin his questline
- Take the lift to the top of the tower, and offer the [Giant](@ex/Giant+of+the+undead+settlement) your friendship, so he will no longer throw spears at you
- At the top of the tower is also a [Soul of a Nameless Soldier](@x) (2000)
- Take the elevator back down and jump off half way, landing on some wooden planks. Leave the door and talk to [Siegward of Catarina](@x) to continue his quest line, then drop down to the burned village
- When you are ready to move on, take the lift down and kill the [Boreal Outrider Knight](@x)
	- he will drop the [Irithyll Straight Sword](@x) on death@ign
- In the same room is a corpse holding an [Ember](@x)

#### Burned Village ####

- Kill the [Fire Demon](@x) to obtain a [Fire Gem](@x)
	- Talk to [Siegward of Catarina](@x) after the fight for a [Siegbrau](@x) and the [Toast](@ex\gestures) gesture@tag{gesture_toast}
	- Talk to [Siegward of Catarina](@x) a second time for the [Sleep](@ex\gestures) gesture@tag{gesture_sleep}
- Near the Destroyed Wagon is a [Homeward Bone](@x)
- A [Large Club](@x) is near a burned out building
- A [Pale Tongue](@x) can be found on a hanging corpse
- The [Northern Armor Set](@x) can be found on another hanging corpse

Continue moving inside the building

- 2 [Red Bug Pellet](@x)s can be found in the first room
- 2 [Alluring Skull](@x)s can be found in the next room. Beware of an ambush from 2 [Cage Spider](@x)s
- Moving across the outside alcove into the next room reveals a chest with 4 [Human Pine Resin](@x). Opening this chest will trigger an ambush by 4 [Cage Spiders](@x)
- [Flynn's Ring](@x) is on the top of the building containing the 2 [Evangelist](@x)s
- Jump down from the building to the tower, there are 2 [Homeward Bone](@x)s on the other side

Carefully jump down into the tower, making sure to land on the wooden beams on the way down

- At the bottom of the tower is a corpse wearing the [Mirrah Armor Set](@x)
- Behind a box is the [Chloranthy Ring](@x)

#### The Grave ####

- Buy the [Grave Key](@x) from the [Shrine Handmaiden](@x) after giving her the [Mortician's Ashes](@x)@tag{grave_key}
- [Loincloth](@x) can be found next to the [Statue of velka](@ex/Velka+the+Goddess+of+Sin)
	- You should pray here if you need to@ign
- [Red Hilted Halberd](@x) is just down the path from the [Statue of velka](@ex/Velka+the+Goddess+of+Sin)
- In the middle of the next room is a [Soul of an Unknown Traveler](@x) (800). Beware of [Root Skeleton](@x)s!

The next room has some skeletons in it, then it leads to an outside area

- Take the left path and kill the [Crystal Liazrd](@x) for a [Heavy Gem](@x)
- Loot the 2 corpses at the end of the area for 2 [Titanite Shard](@x)s. Beware of the [Hollow Manservant](@x) who will fall down and attack you
- Knock down the hanging corpse for a [Blessed Red and White Shield +1](@ex/Red+And+White+Shield)
- There is a [Saint's Talisman](@x) in the sewers area past the shield. Watch out for the [Rat](@ex/Hound-Rat)s!
- Head up the ladder and talk to [Irina of Carim](@x). She will give you the [Prayer](@ex/gestures) Gesture and teleport to Firelink Shrine@tag{gesture_prayer}
	- She is the main miracle trainer@ign
- Open the shortcut door and talk to [Eygon of Carim](@x) to continue his quest line
