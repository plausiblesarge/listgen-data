Once you have the [Tower Key](@x), you can access the roof and the dilapitated tower behind the [Firelink Shrine](@x)

- Pick up the [Soul of a Deserted Corpse](@x) (200) next to the entrance to the dilapitated tower
- Jump down from the tower bridge onto the roof and kick down the shortcut ladder
- Jump down from the roof onto the section next to the entrance to the tower, to pick up 3 [Homeward Bone](@x)s from the roof
- Kill the [Crystal Lizard](@x) behind the tower for a [Twinkling Titanite](@x)
- Pick up the [Estus Shard](@x) next to [Pickle-Pee](@ex/Pickle+Pee,+Pump-a-Rum+Crow)
- Go through the illusory wall at the end of the walkway to find a chest containing the [Covetous Silver Serpent Ring](@x)
- Pick up the [Fire Keeper Soul](@x) from the top of the tower
    - Curing the [Dark Sigil](@x) using the [Fire Keeper Soul](@x) will prevent you from completing [Yoel of Londor](@x)s quest, and will prevent [Yuria of Londor](@x) from appearing.@imp@ign
- Jump down the coffins to get the [Estus Ring](@x)
- Jump down the other side to get the [Fire Keeper Set](@x)
