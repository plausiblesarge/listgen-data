#### Upon starting the game ####

- Turn right and grab the [Soul of a Deserted Corpse](@x) (200)
- Pick up the [Ashen Estus Flask](@x)
- Go up the path on the right and fight the [Ravenous Crystal Lizard](@x) and pick up the [Soul of an Unknown Traveler](@x) (800)
- Activate and use the Cemetery of Ash bonfire and unlock the [Rest](@ex/gestures) gesture@tag{gesture_rest}
- Pick up the 5 [Firebomb](@x)s to make the second-phase [Gundyr](@ex/Iudex+Gundyr) fight easier
- Jump on the Stone chest from the cliff and pick up the [Titanite Shard](@x)
- Kill [Iudex Gundyr](@x) and recieve the [Coiled Sword](@x)
    - Activate the [Iudex gundyr](@x) bonfire

#### After Gundyr ####

Open the door from the [Iudex Gundyr](@x) arena and continue through it

- Pick up the [Broken Straight Sword](@x) at the gravestone directly past the door
- Go down the path to the left and pick up the [Homeward Bone](@x) behind the [Grave Warden](@x)s
- Go up the path to the right and kill the [Starved Hound](@ex/Dog) and pick up the [Ember](@x)
- Kill the 2 [Grave Warden](@x)s above the entrance to the [Firelink Shrine](@x) and pick up the [Ember](@x)
- Pick up the [Homeward Bone](@x) from on top of a tombstone to the right after going above the [Firelink Shrine](@x) entrance
- Kill the [Sword Master](@x) to recieve the [Uchigatana](@x) and the [Master's Set](@x)
- Pick up the [East-West Shield](@x) from the tree near the [Sword Master](@x)
