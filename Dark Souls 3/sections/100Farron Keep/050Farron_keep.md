- Light the Farron Keep bonfire
- A [Flame](@ex/Farron+Keep) is directly in front from the entrance
    - Behind the wall next to the flame is a [Rusted Gold Coin](@x)
    - Just behind the flame is a corpse holding 2 [Homeward Bone](@x)s

#### Left Path ####

- Immediately to the left of the entrance is a [Ragged Mask](@x) guarded by some [Rotten Slug](@x)s
- A [Titanite Shard](@x) can be found on a corpse in the water near the [Ragged Mask](@x)
    - Nearby is a corpse with 10 [Prism Stone](@x)s
- A [Titanite Shard](@x) can be found on the island near the collapsed building
- An [Estus Shard](@x) is right next to the collapsed building
- Up against a nearby tree is a [Stone Parma](@x) guarded by a [Rotten Slug](@x)
- Near another tree is a corpse holding 4 [Rotten Pine Resin](@x)s

Nearby is a tower guarded by [Darkwraith](@x)s

- The [Sage's Coal](@x) is inside the tower
- A corpse with a [Titanite Shard](@x) is on an island nearby
- A second [Titanite Shard](@x) is on a corpse nearby backed up against a broken bridge
    - Next to that bridge is a ramp leading to a [Greatsword](@x)
- From the same island, a stairway leads to a [Flame](@ex/Farron+Keep)
    - Next to the flame is a corpse holding 2 [Purple Moss Clump](@x)s
- Just past this flame is the Keep Ruins bonfire

Past the bonfire are some stairs leading to [The Great Door](@ex/Farron+Keep)

- Next to the door is a small tower filled with [Rotten Slug](@x)s. They are guarding an [Undead Bone Shard](@x)
    - Just past the tower is a ladder leading to the Watchdogs of Farron Tower@ign
    - [Wolf's Blood Swordgrass](@x) is right behind the tower
- On a small island nearby is a [Sunlight Talisman](@x) and some [Estus Soup](@x)
    - On another nearby island are 2 [Titanite Shard](@x) near the [Elder Ghru](@x)s
    - The [Nameless Knight Set](@x) are in a small alcove nearby
    - A [Titanite Shard](@x) is on an island nearby
    - A [Titanite Shard](@x) is under the collapsed bridge nearby
    - On a small island nearby, some [Ghru](@x) guard the [Sage's Scroll](@x)
- The Giant Crab nearby drops the [Lingering Dragoncrest Ring](@x)
    - A White Birch Tree is nearby with 2 [Young White Branch](@x)es at it's base
    - The [Crown of Dusk](@x) is at the foot of the tree
    - [Soul of a Nameless Soldier](@x) is nearby the tree
    - An [Ember](@x) is also nearby
    - 6 [Gold Pine Bundle](@x)s are nearby in the swamp
    - [Large Soul of an Unknown Traveler](@x) is at the base of a nearby tree
- Kill the [Elder Ghru](@x)s to get the [Black Bow of Pharis](@x) and [Pharis's Hat](@x)
    - They are standing around a [Poison Gem](@x)

#### Right Path ####

- Immediately to the right of the bonfire are 3 [Purple Moss Clump](@x)s
- Move into the swamp on the right. [Iron Flesh](@x) is right near a tree
- Past the [Basilisk](@x)s is a small area with a corpse holding a [Large Soul of a Nameless Soldier](@x)

There is a small island afterwards with a [Flame](@ex/Farron+Keep)
    - Behind the Flame is a corpse with 2 [Rotten Pine Resin](@x)s

- Nearby the island, near some [Basilisk](@x)s are 4 [Repair Powder](@x)s
- Defeat [Yellowfinger Heysel](@x)
- Past the Basilisks is a cave containing a [Golden Scroll](@x)
    - Next to it is a chest containing the [Antiquated Set](@x)

#### Watchdogs of Farron Tower and the Bridge ####

- At the top of the tower, behind an illusory wall, is the [Dreamchasers Ashes](@x) 
- A [Crystal Lizard](@x) is at the top of the tower with a [Twinkling Titanite](@x)
- Activate the [Watchdogs of Farron](@x) bonfire
- Approach the wolf to join the [Watchdogs of Farron](@x) covenant and receive the [Legion Etiquette](@ex/gestures) gesture@tag{gesture_legion_etiquette}

Go up the elevator and onto the bridge. 

- Kill the [Stray Demon](@x)
    - He will drop [His Soul](@ex/Soul+of+a+Stray+Demon)@ign
    - Around the Demon are several items@ign
        - 2 [Ember](@x)s
        - A [Greataxe](@x)

There is a secret passage going around the main gate

- 2 [Crystal Lizards](@x) each drop a [Large titanite Shard](@x)
- Another [Crysal Lizard](@x) drops a [Heavy Gem](@x)
- There are many bodies scattered around the area@ign
    - One has the [Dragon Crest Shield](@x)
    - Another has the [Lightning Spear](@x) miracle

#### Through the Doorway ####

- Just through the stone door is a [Shriving Stone](@x)
- Light the [Farron Keep](@x) Perimeter bonfire
- Past the bonfire is a hole in the wall. Through the hole is a cliff face@ign
    - The [Atonement](@x) miracle is at the bottom fo the cliff
    - Next to the [Atonement](@x) miracle is a [Hollow Gem](@x)
- Further down the path is a [Ravenous Crystal Lizard](@x)@ign
    - He will drop a [Scale](@ex/Titanite+Scale) on death
    - Just past the [Ravenous Crystal Lizard](@x) is the [Great Magic Weapon](@x) spell
    - The shortcut to the [Crucifiction Woods](@x) is next to it
- Towards the [Abyss Watchers](@x) fight is a [Ghru](@x) guarding an [Ember](@x)
    - Nearby at the top of a hill, another [Ghru](@x) guards 3 [Black Bug Pellet](@x)s
- At the entrance to the [Abyss Watchers](@x) boss fight, Summon [Black Hand Gotthard](@x) to receive the [By My Sword](@ex/gestures) gesture@tag{gesture_by_my_sword}
- At the entrance to the [Abyss Watchers](@x) boss fight, Summon [Londor Pale Shade](@x) to receive the [Duel Bow](@ex/gestures) gesture@tag{gesture_duel_bow}
- Kill the [Abyss Watchers](@x)
    - @ign@impYou should give the [Golden Scroll](@x) and the [Sage's Scroll](@x) to [Orbeck of Vinheim](@x) prior to defeating the Abyss Watchers otherwise he will disappear forever and his questline will be impossible to complete
        - @imp@ignIf you have not yet recruited [Orbeck of Vinheim](@x) due to a lack of Intelligence (10 required), you can respec at [Rosaria](@ex/Rosaria,+Mother+of+Rebirth)
    - @ign@impIt is also important to complete [Yoel of Londor](@x)'s questline by drawing out your true strength 5 times before defeating the [Abyss Watchers](@x) in order to not fail his questline
    - Light the [Abyss Watchers](@x) bonfire and open the shortcut to the Catacombs of Carthas
    - @ignThey will drop the [Cinders of a Lord](@x) and [Soul of the Blood of the Wolf](@x)
