### Large Room, Upper Level ###

- In the left corner of the first balcony room a corpse holds an [Ember](@x) guarded by a [Smoldering Ghru](@x)
- Around to the right, after the room filled with [Elder Ghru](@x)s is a small hallway with a corpse holding an [Ember](@x).
    - @ignWatch out for the [Demon Statue](@x)
- Jump across the hole for an [Estus Shard](@x).
    - @ignIf you fall down, you can get to the bonfire and the other side of the hole by going up the stairs after the Ghru's at the bottom of the hole.
    - @ignThe [Shard](@ex/Estus+Shard) is accessible through an illusory all in the bonfire room

### Kings Antechamber ###

- Keep going, and the next hallway leads to an Illusory Wall. Jump through it to find the [King's Antechamber Bonfire](@ex/bonfires)@tag{bonfire_kings_antechamber}
    - @ignThis bonfire can also be accessed from the lower level by going through a corridoor at the end of the room and a few rooms filled with enemies, then a flight of stairs. See the map for details
- In the next room, a [Smouldering Ghru](@x) and some [Elder Ghru](@x)s guard the [Izalith Pyromancy Tome](@x)
- Take the long hallway next to the Ghru room and kill the [Crystal Lizard](@x) for a [Chaos Gem](@x)
- Down the stairs from the bonfire room is a maze of corridoors, at the end of a corridoor is a corpse holding an [Ember](@x)
- Through the maze is another room filled with Ghrus and Demon Statues. At the end is an illusory wall leading to a room with a [Black Knight](@x) who guards a [Black Knight Greatsword](@x)@tag{smouldering_lake_black_knight_room}

### Large Room, Lower Level ###

- Go down the stairs and kill all the [Elder Ghru](@x) as they will kill you very quickly@ign
- At the end of the room is a hallway leading to a dead end which is an illusory wall leading to a room with a [Black Knight](@x) who guards a [Black Knight Greatsword](@x)@tag{smouldering_lake_black_knight_room}
- At the end of the room is a hallay filled with [Rats](@ex/Hound-rat) leading to the Basement Area
- In the corner of the room is a [Undead Bone Shard](@x)

### Basement Area ###

- @ignEither drop down the hole or take the hallway into the rat room and kill the rats. The hole comes out in the lava room - be careful
- Open the Illusory Wall in the first room to reveal the [Quelana Pyromancy Tome](@x) and an alternate entrance to the lava room
- Inside the lava room is [???](@x)
- Go down the stairs into the next room and kill the [Giant Rat](@x). Next to the rat is an illusory wall leading to a chest containing 3 [Large Titanite Shard](@x)s 
    - Behind the chest is yet another illusory wall leading to the [Izalith Staff](@x) and an alternate way into the next room.
- Go into the next room. Be careful as it is full of [Basilisks](@x)
- In the corner of the room is a corpse containing a [Titanite Scale](@x)
- This room leads to two rooms. The lava room and the area above the lava room@ign
    - The Lava Room contains [???](@x)
    - The room above the lava room contains a [Black Knight](@x) who is guarding a [Soul of a Crestfallen Knight](@x) (10,000) and leads to the Ballista Area
