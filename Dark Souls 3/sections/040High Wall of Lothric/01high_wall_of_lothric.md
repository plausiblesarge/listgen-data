- Go through the doors and activate the [High Wall of Lothric](@x) bonfire

### Right Path (High Wall of Lothric Bonfire) ###

- At the end of the pathway is a tower with a [Pus of Man](@x) at the top, which drops a [Titanite Shard](@x) on death
- Next to the [Pus of Man](@x) is a [Longbow](@x) and 12 [Standard Arrow](@x)s

To the left from the entrance is a path leading down to a locked door leading to an elevator

- [Soul of a Deserted Corpse](@x) (200) outside the room with the elevator


### Left Path (High Wall of Lothric Bonfire) ###

- Go down the left path and pick up the [Soul of a Deserted Corpse](@x) (200)
- Pick up the [Binoculars](@x) from the battlements of the building with the dead dragon on top of it
- Drop down from the ledge behind the dead dragon and get the 2 [Gold Pine Resin](@x)s
- Pick up the 2 [Firebomb](@x)s in the next room
- Go down the ladder to the dragon bridge, picking up the Soul of a [Deserted Corpse](@x) (200) on the way

### Dragon Bridge ###

- Kill the [Wyvern](@x) for a [Large Titanite Shard](@x)
- loot the [Claymore](@x), [Ember](@x), [Large Soul of a Deserted Corpse](@x) (200) and [Club](@x) from the bridge
- Go through the door at the top of the bridge and kill the [Mimic](@x) for a [Deep Battle Axe](@ex/Battle+Axe)
- Pick up the [Soul of a Deserted Corpse](@x) (200) in the next area next to the [Lothric Knight](@x)

### Tower on the Wall, Second Level ###

- Collect 3 [Firebomb](@x)s on the ledge in the next room, then go up the stairs to the right to activate the Tower on the Wall bonfire
- Pick up the [Titanite Shard](@x) in the corner near the bonfire
- Move back down the stairs, kill the [Assassins](@ex/Hollow+Assassin), and go down the second set of stairs, then go through the archway leading outside. Turn right to find a [Soul of a Deserted Corpse](@x) (200)

Move down the ladder onto the roof. Be careful of the ambush!

- Pick up the 3 [Firebomb](@x)s on the roof next to the praying hollows and the [Pus of Man](@x)
- Kill the [Pus of Man](@x) on the roof for a [Titanite Shard](@x)
- Move over the roof and kill the [Crystal Lizard](@x) for a [Raw Gem](@x)
- Pick up the [Large Soul of a Deserted Corpse](@x) (400) next to the [Crystal Lizard](@x)

Go down the small ladder onto a landing

- Turn left onto the roof above the [Winged Knight](@x) and pick up the 3 [Black Firebombs](@x)
- 3 [Firebomb](@x)s at the end of the path behind the [crossbow hollow](@x)

### Inside the Building ###

#### Small Room on the Left ####

- Smash the jars to reveal 2 [Undead Hunter Charm](@x)s
- In the room behind is a [Titanite Shard](@x). Watch for an ambush!

#### Main Room ####

- Up against the wall is a [Soul of a Deserted Corpse](@x) (200)

#### Through the Corridoor ####

- There is a small path of obstacles, roll through them. On the ledge is a corpse containing 2 [Green Blossom](@x)s
- Drop down and open the chest to find the [Astora Straight Sword](@x)
- Back in the hallway, on the left, a [Hollow Soldier](@x) guards a [Broadsword](@x)

#### Downstairs Room ####

Continue downstairs to a hard room with many enemies. The room contains many useful items. The guy on the upper landing can be very annoying, so he should be dealt with first

- On the upper landing, a chest contains a [Silver Eagle Kite Shield](@x)
- An [Estus Shard](@x) is on the anvil
- A [Titanite Shard](@x) is behind some barrels and boxes
- The [Cell Key](@x) is in a small alcove guarded by a [Starved Hound](@ex/Dog)

### [Winged Knight](@x) area ###

- 2 [Ember](@x)s around the fountain
- A [Rapier](@x) near the sewer pass

### Courtyard After [Winged Knight](@x) ###

- Turn right and kill the [crossbow hollow](@x), go up the stairs, and turn right again to get onto the roof with a [Large Soul of a Deserted Corpse](@x) (400)
- A [Ring of Sacrifice](@x) is on a slat overhang next to this roof
- Continue up the stairs to a courtyard where you will be ambushed by hollows. To the left of the courtyard is a corpse with 3 [Green Blossom](@x)s
- Take the elevator up and unlock the shortcut to the High Wall of Lothric bonfire, and enter the Right Path
- 6 [Throwing Knive](@x)s are at the top leve of the elevator shaft

### Courtyard After [Winged Knight](@x) - Church ###

- [Soul of a Deserted Corpse](@x) (200) to the left as you enter
- [Lucerne](@x) near the single [Lothric Knight](@x)
- [Emma](@x) will give the player a [Small Lothric Banner](@x) and admission to the [Way of Blue](@x) covenant
- Kill the [Red-Eyed Lothric Knight](@ex/Lothric+Knight) for a [Refined Gem](@x)
- Kill the [Vordt of the Boreal Valley](@x) and gain access to the [Undead Settlement](@x)@tag{boss_soul_vordt}
	- You may want to reinforce your [Estus Flask](@x) beforehand@ign
	- Actiavte the [Vordt of the Boreal Valley](@x) bonfire
	- He will drop [His Soul](@ex/soul+of+boreal+valley+vordt)@ign

### Tower on the Wall, Lower Level ###

From the bonfire, go down both sets of stairs and then down the ladder

- In the room after the [Hollow Soldier](@x) on the right is a corpse with 8 [Throwing Knive](@x)s
- [Mail Breaker](@x) is in the next room
- Go and free [Greirat](@x) using the [Cell Key](@x). He will give you a [Blue Tearstone Ring](@x) before teleporting to [Firelink Shrine](@x)

If you have the [Lift Chamber Key](@x), you can open the door down here to face a [Darkwraith](@x)

- Kill the [Darkwraith](@x)
	- He will drop a [Red Eye Orb](@x)@ign
