<?php
$title = "Dark Souls 3 Cheat Sheet";
$desc = "Cheat Sheet for Dark Souls 3. Checklist of things to do, items to get etc.";
$long_desc = "The following is a checklist and set of information I use when playing Dark Souls III to make sure I don't miss
    an item, conversation or boss. I hope you find it useful. A big thanks goes out to the community
    of contributors on the <a href=\"http://darksouls3.wiki.fextralife.com/Dark+Souls+3+Wiki\">Dark Souls 3 Wiki</a> where
    some of this information is borrowed from. If this is your first time visiting this page please check
    out the Help section.";
$long_desc_subtitle = "Warning: Contains Spoilers";
$default_link_location = "http://darksouls3.wiki.fextralife.com/";
$default_link_space_character = "+";
$output = "darksouls3.html";
$template = "template/template.in"; # You shouldn't need to modify this
#$standalone = true;
AddFilter("Soul Items","Soul");
AddFilter("NG+","NG+");
AddFilter("NG++","NG++");
?>
